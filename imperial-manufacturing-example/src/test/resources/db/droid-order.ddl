CREATE TABLE IF NOT EXISTS droid_order_item(
        orderItemId int IDENTITY(1,1) NOT NULL,
        order_number varchar(100) NULL,
        type varchar(100) NULL,
        quantity int NULL,
        destination varchar(100) NULL,
        order_date datetime NULL,
 PRIMARY KEY (orderItemId ASC)
 );
