package com.confluex.examples.imperialmanufacturing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status.Family;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mule.context.notification.NotificationException;
import org.mule.tck.junit4.FunctionalTestCase;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.confluex.examples.imperialmanufacturing.sdk.DroidFactorySdk;
import com.confluex.mule.util.EndpointListener;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

@RunWith(MockitoJUnitRunner.class)
public class DroidFactoryApiFunctionalTest extends FunctionalTestCase {

	InputStream orderXml;
	
	@Mock 
	DroidFactorySdk mockSdk;
	
	JdbcTemplate orderDb;
	
	@Override
	protected String getConfigResources() {
		return "test-spring-config.xml,droid-factory.xml";
	}
	
	@Before
	public void prepareTestData() throws IOException {
		orderXml = new ClassPathResource("ig-88-order.xml").getInputStream();
		when(mockSdk.createOrder()).thenReturn("TEST1");
	}
	
	@Before
	public void injectMockSdk() {
		final DroidManufactureBean bean = muleContext.getRegistry().lookupObject("droidManufacture");
		bean.setDroidFactorySdk(mockSdk);
	}
	
	@Before
	public void lookupJdbc() {
		orderDb = muleContext.getRegistry().lookupObject("droidOrderJdbc");
	}
	
	@Test
	public void postShouldPlaceOrderViaDroidFactorySdk() {
		ClientResponse response = postOrder(orderXml);
		
		assertEquals("Response should have been successful, but was " + response.getClientResponseStatus().getStatusCode(), Family.SUCCESSFUL, response.getClientResponseStatus().getFamily());

		String responseBody = response.getEntity(String.class);
		assertTrue("Response should have included the order number:\n" + responseBody, responseBody.matches(".*<order.*number=\"TEST1\".*"));
	}
	
	@Test
	public void invalidXmlRequestShouldReturn400StatusCode() {
		ClientResponse response = postOrder("<order />"); // invalid, at least one droid element is required
		
		assertEquals(400, response.getClientResponseStatus().getStatusCode());
		String responseBody = response.getEntity(String.class);
		assertTrue("Response body should have contained the original XML:\n" + responseBody, responseBody.matches(".*<order ?/>.*") );
	}
	
	@Test
	public void validOrderShouldBeRecordedInDatabase() throws NotificationException {
		EndpointListener listener = new EndpointListener("updateOrderEndpoint");
		muleContext.registerListener(listener);
		
		postOrder(orderXml);
        //try { Thread.sleep(2000); } catch (InterruptedException e) {} // replace with cleaner way to wait for the asynchronous DB update to complete
		listener.waitForMessage(20000);
		
		List<Map<String, Object>> results = orderDb.queryForList(
				"SELECT type, quantity, destination, order_date FROM droid_order_item WHERE order_number = 'TEST1' ORDER BY quantity DESC ");
		assertEquals( "number of order items", 2, results.size() );
		assertEquals( "first order item destination", "starship:dominion", results.get(0).get("destination") );
		assertEquals( "second order item destination", "planet:orinda", results.get(1).get("destination") );
	}

	// Uses Jersey REST client to send the initial message
	private ClientResponse postOrder(Object body) {
		return Client.create()
				.resource("http://localhost:15101/droid-factory/order")
				.entity(body, MediaType.APPLICATION_XML_TYPE)
				.post(ClientResponse.class);
	}

}
