package com.confluex.mule.util;

import java.util.concurrent.TimeUnit;

import org.mule.api.MuleMessage;
import org.mule.api.context.notification.EndpointMessageNotificationListener;
import org.mule.context.notification.EndpointMessageNotification;
import org.mule.util.concurrent.Latch;

public class EndpointListener implements EndpointMessageNotificationListener<EndpointMessageNotification> {

	final String endpointName;
	final Latch latch = new Latch();
	MuleMessage message;
	
	public EndpointListener(String endpointName) {
		this.endpointName = endpointName;
	}
	
	@Override
	public void onNotification(EndpointMessageNotification notification) {
		if (endpointName.equals(notification.getImmutableEndpoint().getName())) {
			message = notification.getSource();
			latch.release();
		}
	}
	
	public MuleMessage waitForMessage(long timeout) {
		try { latch.await(timeout, TimeUnit.MILLISECONDS); } catch (InterruptedException e) { }
		return message;
	}

}
