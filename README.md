# Confluex Mule Cookbook

The Cookbook is a repository for Mule examples that Confluex can publish, provide to clients, include in training materials, or use to explore specific Mule features.